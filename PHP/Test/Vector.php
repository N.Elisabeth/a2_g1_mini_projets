<?php

class Vector {


    private $x;
    private $y;


    public static function Left(){
        return new Vector(-1, 0);
    }

    public static function Right(){
        return new Vector(1, 0);
    }

    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function getX()
    {
        return $this->x;
    }

    public function getY()
    {
        return $this->y;
    }
}

$vectorLeft = Vector::Left();
$vectorRight = new Vector(-1, 0);
$vectorRight = Vector::Right();

class Color {

    private $r;
    private $g;
    private $b;
    private $a;

    public static function RED(){
        return new Color(255, 0, 0, 0);
    }
    public static function GREEN(){
        return new Color(0, 255, 0, 0);
    }

    /**
     * @param $colorA
     * @param $colorB
     * @param $t
     * @return Color
     */
    public static function Lerp(Color $colorA, Color $colorB, $t): Color
    {
        return new Color(
                $colorA->getR() + $t * ( $colorB->getR() - $colorA->getR()),
            $colorA->getG() + $t * ( $colorB->getG() - $colorA->getG()),
            $colorA->getB() + $t * ( $colorB->getB() - $colorA->getB()),
            $colorA->getA() + $t * ( $colorB->getA() - $colorA->getA()));
    }

    public function __construct($r, $g, $b, $a)
    {
        $this->r = $r;
        $this->g = $g;
        $this->b = $b;
        $this->a = $a;
    }

    public function getA()
    {
        return $this->a;
    }

    public function getB()
    {
        return $this->b;
    }

    public function getG()
    {
        return $this->g;
    }

    public function getR()
    {
        return $this->r;
    }

}

$red = new Color(255, 0,0, 0);
$red = Color::RED();
$green = Color::GREEN();

$middle = Color::Lerp($red, $green, 1);
var_dump($middle);