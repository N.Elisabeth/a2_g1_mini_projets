<?php
require_once "Utility/Renderer.php";

require_once "Models/Game.php";





$game = new Game();

if(array_key_exists("resetValue", $_POST)){
    $game->resetGame();

    $keys = array_keys($_POST);
    foreach ($keys as $key){
        unset($_POST[$key]);
    }

}

if(array_key_exists("pos_x", $_POST) && array_key_exists("pos_y", $_POST) && array_key_exists("overlay_type", $_POST)){
    $game->playTurn(new Vector2($_POST["pos_x"], $_POST["pos_y"]));
    echo "ALED";
}

?>

<html>
<head>
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>

<?php
var_dump($_POST);
echo Renderer::getDom()->saveHTML($game->render());
?>
<form method="post">
    <input type="submit" value="Reset">
    <input type="number" name="resetValue" value="true" style="display: none">
</form>

</body>
</html>

