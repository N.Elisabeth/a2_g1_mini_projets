<?php


class Renderer
{

    public static $_instance;

    private $dom;

    public function __construct()
    {
        $this->dom = new DOMDocument();
    }

    /**
     * @return mixed
     */
    public static function getDom() : DOMDocument
    {
        if(self::$_instance == null){
            self::$_instance = new Renderer();
        }

        return self::$_instance->dom;
    }


    public static function generateInput($type, $name, $value = "", $style = "", $class = ""){
        $input = self::getDom()->createElement("input");
        $input->setAttribute("type", $type);
        $input->setAttribute("name", $name);
        $input->setAttribute("value", $value);
        $input->setAttribute("style", $style);
        $input->setAttribute("class", $class);

        return $input;
    }

}