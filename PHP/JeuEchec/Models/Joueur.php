<?php

class Joueur
{

    /**
     * @var Piece []
     */
    private $pieces;

    /**
     * @var Piece []
     */
    private $losePieces;

    /*
     * FALSE => Tour des blanc
     * TRUE => Tour des noir
    */
    private $flag;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Vector2
     *
     * => Vector2 == -1, -1 => Alors c'est que y à pas de sélection
     *
     */
    private $selectedCase;

    public function __construct($flag, $name = "")
    {
        $this->flag = $flag;
        $this->name = $name;

        $this->generatePieces();

        $color = $this->flag ? "black" : "white";
        if(array_key_exists("player_".$color."_x", $_COOKIE) && array_key_exists("player_".$color."_y", $_COOKIE)){
            $this->selectedCase = new Vector2($_COOKIE["player_".$color."_x"], $_COOKIE["player_".$color."_y"]);
        }else{
            $this->setSelectedCase(Vector2::negatif());
        }
    }

    public function isSelectedCase(){
        // si c'est égale le vecteur est donc à -1 -1 donc y à pas de selection donc false
        return !Vector2::Equals($this->selectedCase, Vector2::negatif());
    }

    /**
     * @return Vector2
     */
    public function getSelectedCase(): Vector2
    {
        return $this->selectedCase;
    }

    /**
     * @return Piece[]
     */
    public function getPieces(): array
    {
        return $this->pieces;
    }

    /**
     * @param Vector2 $selectedCase
     */
    public function setSelectedCase(Vector2 $selectedCase): void
    {
        $this->selectedCase = $selectedCase;
        $color = $this->flag ? "black" : "white";
        setcookie("player_".$color."_x", $selectedCase->getX(), time() + 3600, "/");
        setcookie("player_".$color."_y", $selectedCase->getY(), time() + 3600, "/");
    }


    private function generatePieces(){
        $tempPieces = [];

        $place = $this->flag ? 6 : 1;
        for($x = 0; $x < Plateau::SIZE; $x++){
            array_push($tempPieces, new Pion($x,$place, $this->flag));
        }
        $place = $this->flag ? 7 : 0;
        array_push($tempPieces, new Roi(3,$place, $this->flag));
        array_push($tempPieces, new Reine(4,$place, $this->flag));
        array_push($tempPieces, new Fou(2,$place, $this->flag));
        array_push($tempPieces, new Fou(5,$place, $this->flag));
        array_push($tempPieces, new Chavalier(1,$place, $this->flag));
        array_push($tempPieces, new Chavalier(6,$place, $this->flag));
        array_push($tempPieces, new Tour(0,$place, $this->flag));
        array_push($tempPieces, new Tour(7,$place, $this->flag));

        $this->pieces = [];
        $this->losePieces = [];

        foreach ($tempPieces as $piece) {
            if($piece->isAlive()){
                array_push($this->pieces, $piece);
            }else{
                array_push($this->losePieces, $piece);
            }
        }
    }

    function reset(){
        $this->setSelectedCase(Vector2::negatif());
        foreach ($this->pieces as $piece){
            $piece->reset();
        }
        foreach ($this->losePieces as $piece){
            $piece->reset();
        }
    }

}