<?php


interface ILMouvement
{
    public function calculLMouvement(Plateau $plateau): array;
}