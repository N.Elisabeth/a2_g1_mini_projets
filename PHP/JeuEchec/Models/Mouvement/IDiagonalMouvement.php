<?php


interface IDiagonalMouvement
{
    public function calculDiagonalMouvement(Plateau $plateau): array;
}