<?php


interface IHorizontalMouvement
{
    public function calculHorizontalMouvement(Plateau $plateau): array;
}