<?php


interface IVerticalMouvement
{
    public function calculVerticalMouvement(Plateau $plateau): array;
}