<?php

require_once "./Models/Mouvement/IDiagonalMouvement.php";
require_once "./Models/Mouvement/IVerticalMouvement.php";
require_once "./Models/Mouvement/IHorizontalMouvement.php";

class Roi extends Piece implements  IVerticalMouvement, IHorizontalMouvement, IDiagonalMouvement
{

    public function render(): DOMElement
    {
        return parent::draw("king");
    }
    public function calculHorizontalMouvement(Plateau $plateau): array
    {
        $overlays = [];

        $x = 1;
        if($this->position->getX() + $x < Plateau::SIZE){
            $overlay = parent::checkCase(new Vector2($this->position->getX() + $x,$this->position->getY()), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
            }
        }

        $x = -1;
        if(0 <= $this->position->getX() + $x){
            $overlay = parent::checkCase(new Vector2($this->position->getX() + $x,$this->position->getY()), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
            }
        }

        return $overlays;
    }

    public function calculVerticalMouvement(Plateau $plateau): array
    {
        $overlays = [];

        $y = 1;
        if($this->position->getY() + $y < Plateau::SIZE){
            $overlay = parent::checkCase(new Vector2($this->position->getX(),$this->position->getY() + $y), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
            }
        }

        $y = -1;
        if(0 <= $this->position->getY() + $y){
            $overlay = parent::checkCase(new Vector2($this->position->getx(),$this->position->getY() + $y), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
            }
        }

        return $overlays;
    }

    public function calculDiagonalMouvement(Plateau $plateau): array
    {
        $overlays = [];

        $i = 1;
        if($this->position->getX() + $i < Plateau::SIZE && $this->position->getY() + $i < Plateau::SIZE){
            $overlay = parent::checkCase(new Vector2($this->position->getX() + $i,$this->position->getY() + $i), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
            }
        }

        $i = 1;
        if(0 <= $this->position->getX() - $i && $this->position->getY() + $i < Plateau::SIZE){
            $overlay = parent::checkCase(new Vector2($this->position->getX() - $i,$this->position->getY() + $i), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
            }
        }

        $i = 1;
        if($this->position->getX() + $i < Plateau::SIZE && 0 <= $this->position->getY() - $i){
            $overlay = parent::checkCase(new Vector2($this->position->getX() + $i,$this->position->getY() - $i), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);

            }
        }

        $i = 1;
        if(0 <= $this->position->getX() - $i && 0 <= $this->position->getY() - $i){
            $overlay = parent::checkCase(new Vector2($this->position->getX() - $i,$this->position->getY() - $i), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
            }
        }

        return $overlays;
    }

    protected function getName(): string
    {
        $color = $this->flag ? "black" : "white";
        return "piece_".$color."_roi";
    }

    public function getPossibleMouv(Plateau $plateau): array
    {
        return array_merge($this->calculVerticalMouvement($plateau), $this->calculHorizontalMouvement($plateau), $this->calculDiagonalMouvement($plateau));
    }
}