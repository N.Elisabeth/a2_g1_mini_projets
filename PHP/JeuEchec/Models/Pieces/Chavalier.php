<?php

require_once "./Models/Mouvement/ILMouvement.php";

class Chavalier extends Piece implements ILMouvement
{
    private $startX;

    public function __construct(int $x, int $y, bool $flag = false)
    {
        $this->startX = $x;
        parent::__construct($x, $y, $flag);
    }

    protected function getName(): string
    {
        $color = $this->flag ? "black" : "white";
        return "piece_".$color."_tour_".$this->startX;
    }

    public function render(): DOMElement
    {
        return parent::draw("knight");
    }

    public function getPossibleMouv(Plateau $plateau): array
    {
        return $this->calculLMouvement($plateau);
    }

    public function calculLMouvement(Plateau $plateau): array
    {
        $overlays = [];

        $mouvList = [
            new Vector2(2, 1),
            new Vector2(2, -1),
            new Vector2(-2, 1),
            new Vector2(-2, -1),
            new Vector2(1, 2),
            new Vector2(-1, 2),
            new Vector2(1, -2),
            new Vector2(-1, -2),
        ];

        foreach ($mouvList as $mouv){
            if(
                (0 <= $this->position->getX() + $mouv->getX() && $this->position->getX() + $mouv->getX() < Plateau::SIZE) &&
                (0 <= $this->position->getY() + $mouv->getY() && $this->position->getY() + $mouv->getY() < Plateau::SIZE)
            ){
                $overlay = parent::checkCase(new Vector2($this->position->getX() + $mouv->getX(),$this->position->getY() + $mouv->getY()), $plateau);
                if($overlay != null){
                    array_push($overlays, $overlay);
                }
            }
        }

        return $overlays;
    }
}