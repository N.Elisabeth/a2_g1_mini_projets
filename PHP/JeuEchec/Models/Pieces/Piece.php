<?php


abstract class Piece
{

    /**
     * @var bool
     */
    protected $flag;

    /**
     * @var Vector2
     */
    protected $position;

    /**
     * @var bool // True => En vie || False => Mort
     */
    private $alive;

    public function __construct(int $x, int $y, bool $flag = false)
    {
        $this->flag = $flag;

        if(array_key_exists($this->getName()."_x", $_COOKIE) && array_key_exists($this->getName()."_y", $_COOKIE)){
            $this->position = new Vector2($_COOKIE[$this->getName()."_x"], $_COOKIE[$this->getName()."_y"]);
        }else{
            $this->setPosition(new Vector2($x, $y));
        }

        if(array_key_exists($this->getName()."_alive", $_COOKIE)){
            $this->alive = (bool) $_COOKIE[$this->getName()."_alive"];
        }else{
            $this->setAlive(1);
        }
    }

    /**
     * @param bool $alive
     */
    public function setAlive(int $alive): void
    {
        $this->alive = (bool) $alive;
        setcookie($this->getName()."_alive", $alive, time() + 3600, "/");
    }

    /**
     * @return bool
     */
    public function isAlive()
    {
        return $this->alive;
    }

    /**
     * @return Vector2
     */
    public function getPosition(): Vector2
    {
        return $this->position;
    }

    /**
     * @param Vector2 $position
     */
    public function setPosition(Vector2 $position): void
    {
        $this->position = $position;
        setcookie($this->getName()."_x", $position->getX(), time() + 3600, "/");
        setcookie($this->getName()."_y", $position->getY(), time() + 3600, "/");
    }


    public function reset(){
        setcookie($this->getName()."_x", -1, time() - 3600, "/");
        setcookie($this->getName()."_y", -1, time() - 3600, "/");
        setcookie($this->getName()."_alive", 0, time() - 3600, "/");
    }


    /**
     * @return bool
     */
    public function isFlag(): bool
    {
        return $this->flag;
    }


    /**
     * @param Plateau $plateau
     * @return Overlay[]
     */
    public abstract function getPossibleMouv(Plateau $plateau): array;

    /**
     * @return string
     */
    protected abstract function getName() : string;

    /**
     * @return DOMElement
     */
    public abstract function render(): DOMElement;

    public function draw($pieceName): DOMElement
    {
        $div = Renderer::getDom()->createElement("img");
        $div->setAttribute("class", "piece");
        $div->setAttribute("src", "assets/img/chess-".($this->flag ? "black" : "white")."-".$pieceName."-solid.svg");
        return $div;
    }

    protected function checkCase(Vector2 $position, Plateau $plateau) : ?Overlay
    {
        $tile = $plateau->getTile($position->getX(), $position->getY());
        if($tile != null){
            if($tile->getPiece() != null){
                if($tile->getPiece()->isFlag() == $this->flag){
                    return null;
                    // return new Overlay($position, OverlayType::ALLIE);
                }
                return new Overlay($position, OverlayType::DANGER);
            }
            return new Overlay($position, OverlayType::MOUV);
        }
        return null;
    }


}