<?php

require_once "./Models/Mouvement/IDiagonalMouvement.php";
require_once "./Models/Mouvement/IVerticalMouvement.php";
require_once "./Models/Mouvement/IHorizontalMouvement.php";

class Reine extends Piece implements IVerticalMouvement, IHorizontalMouvement, IDiagonalMouvement
{
    public function render(): DOMElement
    {
        return parent::draw("queen");
    }

    public function calculHorizontalMouvement(Plateau $plateau): array
    {
        $overlays = [];

        $x = 1;
        while($this->position->getX() + $x < Plateau::SIZE){
            $overlay = parent::checkCase(new Vector2($this->position->getX() + $x,$this->position->getY()), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
                if($overlay->getOverlayType() == OverlayType::DANGER || $overlay->getOverlayType() == OverlayType::ALLIE){
                    break;
                }
            }else{
                break;
            }
            $x++;
        }

        $x = -1;
        while(0 <= $this->position->getY() + $x){
            $overlay = parent::checkCase(new Vector2($this->position->getX() + $x,$this->position->getY()), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
                if($overlay->getOverlayType() == OverlayType::DANGER || $overlay->getOverlayType() == OverlayType::ALLIE){
                    break;
                }
            }else{
                break;
            }
            $x--;
        }

        return $overlays;
    }

    public function calculVerticalMouvement(Plateau $plateau): array
    {
        $overlays = [];

        $y = 1;
        while($this->position->getY() + $y < Plateau::SIZE){
            $overlay = parent::checkCase(new Vector2($this->position->getX(),$this->position->getY() + $y), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
                if($overlay->getOverlayType() == OverlayType::DANGER || $overlay->getOverlayType() == OverlayType::ALLIE){
                    break;
                }
            }else{
                break;
            }
            $y++;
        }

        $y = -1;
        while(0 <= $this->position->getY() + $y){
            $overlay = parent::checkCase(new Vector2($this->position->getx(),$this->position->getY() + $y), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
                if($overlay->getOverlayType() == OverlayType::DANGER || $overlay->getOverlayType() == OverlayType::ALLIE){
                    break;
                }
            }else{
                break;
            }
            $y--;
        }

        return $overlays;
    }

    public function calculDiagonalMouvement(Plateau $plateau): array
    {
        $overlays = [];

        $i = 1;
        while($this->position->getX() + $i < Plateau::SIZE && $this->position->getY() + $i < Plateau::SIZE){
            $overlay = parent::checkCase(new Vector2($this->position->getX() + $i,$this->position->getY() + $i), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
                if($overlay->getOverlayType() == OverlayType::DANGER || $overlay->getOverlayType() == OverlayType::ALLIE){
                    break;
                }
            }else{
                break;
            }
            $i++;
        }

        $i = 1;
        while(0 <= $this->position->getX() - $i && $this->position->getY() + $i < Plateau::SIZE){
            $overlay = parent::checkCase(new Vector2($this->position->getX() - $i,$this->position->getY() + $i), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
                if($overlay->getOverlayType() == OverlayType::DANGER || $overlay->getOverlayType() == OverlayType::ALLIE){
                    break;
                }
            }else{
                break;
            }
            $i++;
        }

        $i = 1;
        while($this->position->getX() + $i < Plateau::SIZE && 0 <= $this->position->getY() - $i){
            $overlay = parent::checkCase(new Vector2($this->position->getX() + $i,$this->position->getY() - $i), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
                if($overlay->getOverlayType() == OverlayType::DANGER || $overlay->getOverlayType() == OverlayType::ALLIE){
                    break;
                }
            }else{
                break;
            }
            $i++;
        }

        $i = 1;
        while(0 <= $this->position->getX() - $i && 0 <= $this->position->getY() - $i){
            $overlay = parent::checkCase(new Vector2($this->position->getX() - $i,$this->position->getY() - $i), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
                if($overlay->getOverlayType() == OverlayType::DANGER || $overlay->getOverlayType() == OverlayType::ALLIE){
                    break;
                }
            }else{
                break;
            }
            $i++;
        }

        return $overlays;
    }

    public function getPossibleMouv(Plateau $plateau): array
    {
        return array_merge($this->calculVerticalMouvement($plateau), $this->calculHorizontalMouvement($plateau), $this->calculDiagonalMouvement($plateau));
    }

    protected function getName(): string
    {
        $color = $this->flag ? "black" : "white";
        return "piece_".$color."_rene";
    }
}