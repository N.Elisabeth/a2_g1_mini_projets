<?php

require_once "./Models/Mouvement/IDiagonalMouvement.php";

class Fou extends Piece implements IDiagonalMouvement
{
    private $startX;

    public function __construct(int $x, int $y, bool $flag = false)
    {
        $this->startX = $x;
        parent::__construct($x, $y, $flag);
    }

    protected function getName(): string
    {
        $color = $this->flag ? "black" : "white";
        return "piece_".$color."_tour_".$this->startX;
    }

    public function render(): DOMElement
    {
        return parent::draw("bishop");
    }

    public function getPossibleMouv(Plateau $plateau): array
    {
        return  $this->calculDiagonalMouvement($plateau);
    }

    public function calculDiagonalMouvement(Plateau $plateau): array
    {
        $overlays = [];

        $i = 1;
        while($this->position->getX() + $i < Plateau::SIZE && $this->position->getY() + $i < Plateau::SIZE){
            $overlay = parent::checkCase(new Vector2($this->position->getX() + $i,$this->position->getY() + $i), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
                if($overlay->getOverlayType() == OverlayType::DANGER || $overlay->getOverlayType() == OverlayType::ALLIE){
                    break;
                }
            }else{
                break;
            }
            $i++;
        }

        $i = 1;
        while(0 <= $this->position->getX() - $i && $this->position->getY() + $i < Plateau::SIZE){
            $overlay = parent::checkCase(new Vector2($this->position->getX() - $i,$this->position->getY() + $i), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
                if($overlay->getOverlayType() == OverlayType::DANGER || $overlay->getOverlayType() == OverlayType::ALLIE){
                    break;
                }
            }else{
                break;
            }
            $i++;
        }

        $i = 1;
        while($this->position->getX() + $i < Plateau::SIZE && 0 <= $this->position->getY() - $i){
            $overlay = parent::checkCase(new Vector2($this->position->getX() + $i,$this->position->getY() - $i), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
                if($overlay->getOverlayType() == OverlayType::DANGER || $overlay->getOverlayType() == OverlayType::ALLIE){
                    break;
                }
            }else{
                break;
            }
            $i++;
        }

        $i = 1;
        while(0 <= $this->position->getX() - $i && 0 <= $this->position->getY() - $i){
            $overlay = parent::checkCase(new Vector2($this->position->getX() - $i,$this->position->getY() - $i), $plateau);
            if($overlay != null){
                array_push($overlays, $overlay);
                if($overlay->getOverlayType() == OverlayType::DANGER || $overlay->getOverlayType() == OverlayType::ALLIE){
                    break;
                }
            }else{
                break;
            }
            $i++;
        }

        return $overlays;
    }
}