<?php

require_once "Piece.php";
require_once "./Models/Mouvement/IVerticalMouvement.php";
require_once "./Models/Mouvement/IDiagonalMouvement.php";

class Pion extends Piece implements IVerticalMouvement, IDiagonalMouvement
{
    /**
     * @var Vector2
     */
    private $startPos;

    public function __construct(int $x, int $y, bool $flag = false)
    {
        $this->startPos = new Vector2($x, $y);
        parent::__construct($x, $y, $flag);
    }


    public function render():DOMElement
    {
        return parent::draw("pawn");
    }

    public function getPossibleMouv(Plateau $plateau): array
    {
        return array_merge($this->calculVerticalMouvement($plateau), $this->calculDiagonalMouvement($plateau));
    }



    public function calculVerticalMouvement(Plateau $plateau): array
    {
        $overlays = [];

        $y = $this->flag ? -1 : 1;
        if(0 <= $this->position->getY() + $y && $this->position->getY() + $y < Plateau::SIZE){
            $overlay = parent::checkCase(new Vector2($this->position->getX(),$this->position->getY() + $y), $plateau);
            if($overlay != null && $overlay->getOverlayType() != OverlayType::DANGER){
                array_push($overlays, $overlay);

                if(Vector2::Equals($this->startPos, $this->position)){
                    if(0 <= $this->position->getY() + $y *2 && $this->position->getY() + $y*2 < Plateau::SIZE){
                        $overlay = parent::checkCase(new Vector2($this->position->getX(),$this->position->getY() + $y*2), $plateau);
                        if($overlay != null && $overlay->getOverlayType() != OverlayType::DANGER){
                            array_push($overlays, $overlay);
                        }
                    }
                }
            }
        }



        return $overlays;
    }

    public function calculDiagonalMouvement(Plateau $plateau): array
    {
        $overlays = [];

        $y = $this->flag ? -1 : 1;
        $mouvList = [-1, 1];

        foreach ($mouvList as $mouv){

            if(
                (0 <= $this->position->getX() + $mouv && $this->position->getX() + $mouv < Plateau::SIZE) &&
                (0 <= $this->position->getY() + $y && $this->position->getY() + $y < Plateau::SIZE)
            ){
                $overlay = parent::checkCase(new Vector2($this->position->getX() + $mouv,$this->position->getY() + $y), $plateau);
                if($overlay != null && $overlay->getOverlayType() != OverlayType::MOUV){
                    array_push($overlays, $overlay);
                }
            }


        }
        return $overlays;
    }

    protected function getName(): string
    {
        $color = $this->flag ? "black" : "white";
        return "piece_".$color."_pion_".$this->startPos->getX();
    }
}