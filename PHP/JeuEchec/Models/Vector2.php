<?php


class Vector2
{

    private $x;
    private $y;

    public static function negatif(): Vector2
    {
        return new Vector2(-1, -1);
    }

    public static function zero(): Vector2
    {
        return new Vector2(0, 0);
    }


    public function __construct(int $x, int $y)
    {
        $this->y = $y;
        $this->x = $x;
    }

    public static function Equals(Vector2 $vectorA, Vector2 $vectorB):bool
    {
        return $vectorA->getX() == $vectorB->getX() && $vectorA->getY() == $vectorB->getY();
    }


    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->y;
    }
}