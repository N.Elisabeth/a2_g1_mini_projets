<?php

require_once "Vector2.php";
require_once "./Models/Overlay/Overlay.php";
// Case == Tile
class Tile
{

    /**
     * @var Vector2
     */
    private $position;
    /**
     * @var bool
     */
    private $flag;

    /**
     * @var Piece
     */
    private $piece;

    /**
     * @var Overlay
     */
    private $overlay;

    public function __construct(int $x, int $y, bool $flag)
    {
        $this->position = new Vector2($x, $y);
        $this->flag = $flag;
        $this->piece = null;
        $this->overlay = null;
    }

    /**
     * @param Piece $piece
     */
    public function setPiece(?Piece $piece): void
    {
        $this->piece = $piece;
    }

    /**
     * @return Piece
     */
    public function getPiece(): ?Piece
    {
        return $this->piece;
    }

    /**
     * @return Overlay
     */
    public function getOverlay(): ?Overlay
    {
        return $this->overlay;
    }

    /**
     * @param Overlay $overlay
     */
    public function setOverlay(?Overlay $overlay): void
    {
        $this->overlay = $overlay;
    }

    public function render(bool $turn, Plateau $plateau) : DOMElement
    {
        // , "X:".$this->position->getX()."-Y:".$this->position->getY()
        $caseDiv = Renderer::getDom()->createElement("div");
        $caseDiv->setAttribute("class", "tile");
        $caseDiv->setAttribute("style", "background-color:".($this->flag ? "lightgray": "darkgray"));

        if($this->piece != null){
            $caseDiv->appendChild($this->piece->render());

            $mouv = $this->piece->getPossibleMouv($plateau);
            if(count($mouv) != 0 && $this->overlay == null && $turn == $this->piece->isFlag()){
                $this->overlay = new Overlay(new Vector2(0,0), OverlayType::TAKABLE);
            }
        }

        if($this->overlay != null){
            $overlayDiv = $this->overlay->render();
            if($this->piece != null){
                $overlayDiv->setAttribute("style", "top:-68px;".$overlayDiv->getAttribute("style"));
            }

            if($this->overlay->getOverlayType() != OverlayType::SELECT){
                $form = Renderer::getDom()->createElement("form");
                $form->setAttribute("method", "POST");

                $positionX = Renderer::generateInput("number", "pos_x", $this->position->getX(), "display:none");
                $positionY = Renderer::generateInput("number", "pos_y", $this->position->getY(), "display:none");
                $overlay = Renderer::generateInput("text", "overlay_type", $this->overlay->getOverlayType(), "display:none");
                $submit = Renderer::generateInput("submit", "submit", "", "background:none; width: 100%; height:100%; border:none");

                $form->appendChild($overlay);
                $form->appendChild($positionX);
                $form->appendChild($positionY);
                $form->appendChild($submit);

                $overlayDiv->appendChild($form);
            }

            $caseDiv->appendChild($overlayDiv);
        }

        return $caseDiv;
    }

}