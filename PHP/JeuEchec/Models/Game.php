<?php

require_once "Models/Plateau.php";
require_once "Models/Pieces/Pion.php";
require_once "Models/Pieces/Roi.php";
require_once "Models/Pieces/Fou.php";
require_once "Models/Pieces/Chavalier.php";
require_once "Models/Pieces/Tour.php";
require_once "Models/Pieces/Reine.php";
require_once "Models/Joueur.php";

class Game
{
    /**
     * @var bool
     */
    private $turn;
    /**
     * @var Joueur
     */
    private $joueurBlanc;

    /**
     * @var Joueur
     */
    private $joueurNoir;

    /**
     * @var Plateau
     */
    private $plateau;

    /**
     * @var Joueur
     */
    private $currentJoueur;

    public function __construct()
    {

        $this->plateau = new Plateau();

        $this->joueurBlanc = new Joueur(false);
        foreach ($this->joueurBlanc->getPieces() as $piece){
            $this->plateau->getTile($piece->getPosition()->getX(), $piece->getPosition()->getY())->setPiece($piece);
        }


        $this->joueurNoir = new Joueur(true);
        foreach ($this->joueurNoir->getPieces() as $piece){
            $this->plateau->getTile($piece->getPosition()->getX(), $piece->getPosition()->getY())->setPiece($piece);
        }

        $this->turn = 0;
        if(array_key_exists("turn", $_COOKIE) == false){
            setcookie("turn", 0, time() + 3600, "/");
            $this->currentJoueur = $this->joueurBlanc;
        }else{
            $this->turn = (bool) $_COOKIE["turn"];
            $this->currentJoueur = $this->turn ? $this->joueurNoir : $this->joueurBlanc;
        }
    }


    public function playTurn(Vector2 $playedPosition){
        $currentCase = $this->plateau->getTile($playedPosition->getX(), $playedPosition->getY());
        // Si le joueur il à déjà une case sélectionner
        if(
            $this->currentJoueur->isSelectedCase() &&
            Vector2::Equals(new Vector2($playedPosition->getX(),$playedPosition->getY()), $this->currentJoueur->getSelectedCase()) == false &&
            $_POST["overlay_type"] != OverlayType::TAKABLE
        ){
            $oldCase = $this->plateau->getTile($this->currentJoueur->getSelectedCase()->getX(), $this->currentJoueur->getSelectedCase()->getY());

            $piece = $oldCase->getPiece();
            $piece->setPosition(new Vector2($playedPosition->getX(),$playedPosition->getY()));
            $oldCase->setPiece(null);


            if($currentCase->getPiece() != null){
                $currentCase->getPiece()->setAlive(0);
            }
            $currentCase->setPiece($piece);

            $this->currentJoueur->setSelectedCase(Vector2::negatif());

            $this->turn = !$this->turn;
            setcookie("turn", (int) $this->turn, time() + 3600, "/");

        }else{
            $piece = $this->plateau->getTile($playedPosition->getX(),$playedPosition->getY())->getPiece();
            if($piece != null){
                $this->currentJoueur->setSelectedCase(new Vector2($playedPosition->getX(),$playedPosition->getY()));
                $this->plateau->getTile($playedPosition->getX(), $playedPosition->getY())->setOverlay(new Overlay(new Vector2($_POST["pos_x"],$_POST["pos_y"]), OverlayType::SELECT));
                $overlays = $piece->getPossibleMouv($this->plateau);
                foreach ($overlays as $overlay){
                    $this->plateau->getTile($overlay->getPosition()->getX(), $overlay->getPosition()->getY())->setOverlay($overlay);
                }
            }
        }
    }

    public function render():DOMElement{
        return $this->plateau->renderBoard($this->turn);
    }

    public function resetGame(){
        setcookie("turn", 0, time() - 3600, "/");
        $this->joueurBlanc->reset();
        $this->joueurNoir->reset();
    }
}