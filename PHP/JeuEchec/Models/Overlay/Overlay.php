<?php

require_once "OverlayType.php";

class Overlay
{
    /**
     * @var Vector2
     */
    private $position;

    /**
     * @var string
     */
    private $overlayType;

    public function __construct($position, $overlayType)
    {
        $this->position = $position;
        $this->overlayType = $overlayType;
    }

    /**
     * @return Vector2
     */
    public function getPosition(): Vector2
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function getOverlayType(): string
    {
        return $this->overlayType;
    }


    public function render(): DOMElement
    {
        $div = Renderer::getDom()->createElement("div");
        $div->setAttribute("class", "overlay");

        switch ($this->overlayType){
            case OverlayType::SELECT:
                $div->setAttribute("style", "background-color: #0000ff4f; border-color: blue");
                break;
            case OverlayType::DANGER:
                $div->setAttribute("style", "background-color: #ff00004f; border-color: red");
                break;
            case OverlayType::MOUV:
                $div->setAttribute("style", "background-color: #00ff004f; border-color: green");
                break;
            case OverlayType::ALLIE:
                $div->setAttribute("style", "background-color: #0000004f; border-color: black");
                break;
            case OverlayType::TAKABLE:
                $div->setAttribute("style", "background-color: #fae7724f; border-color: #fae772");
                break;
        }


        return $div;
    }


}