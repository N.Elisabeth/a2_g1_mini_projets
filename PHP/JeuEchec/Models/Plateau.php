<?php

require_once "Tile.php";

class Plateau
{
    public const SIZE = 8;
    /**
     * @var Tile[][]
     */
    private $board;

    public function __construct()
    {
        $this->board = $this->generateBoard();
    }

    public function getTile(int $x, int $y){
        return $this->board[$y][$x];
    }


    private function generateLine($lineIndex)
    {
        $line = [];
        for($x = 0; $x < self::SIZE; $x++){
            array_push($line, new Tile($x, $lineIndex, ($lineIndex %2 == 0 && $x%2 == 0) || ($lineIndex %2 == 1 && $x%2 == 1)));
        }
        return $line;
    }

    private function generateBoard()
    {
        $board = [];
        for($y = 0; $y < self::SIZE; $y++){
            array_push($board, $this->generateLine($y));
        }
        return $board;
    }


    private function renderLine($lineIndex, bool $turn): DOMElement
    {
        $lineDiv = Renderer::getDom()->createElement("div");
        $lineDiv->setAttribute("class", "row");

        for($x = 0; $x < self::SIZE; $x++){
            if(0 <= $lineIndex && $lineIndex < self::SIZE){
                $lineDiv->appendChild($this->board[$lineIndex][$x]->render($turn, $this));
            }else{
                $caseDiv = Renderer::getDom()->createElement("div", $x);
                $caseDiv->setAttribute("class", "tile decorateur");
                $caseDiv->setAttribute("style", "background-color:".($x%2 == (int) $turn ? "lightgray": "darkgray"));
                $lineDiv->appendChild($caseDiv);
            }
        }
        return  $lineDiv;
    }

    private function renderCol(bool $turn): DOMElement
    {
        $letters = ["A", "B", "C", "D", "E", "F", "G", "H"];

        $colDiv = Renderer::getDom()->createElement("div");
        $colDiv->setAttribute("class", "col");

        for($x = 0; $x < self::SIZE; $x++){
            $caseDiv = Renderer::getDom()->createElement("div", $letters[$x]);
            $caseDiv->setAttribute("class", "tile decorateur");
            $caseDiv->setAttribute("style", "background-color:".($x%2 == (int) $turn ? "lightgray": "darkgray"));
            $colDiv->appendChild($caseDiv);
        }
        return  $colDiv;
    }

    public function renderBoard(bool $turn):DOMElement{
        $boardManagerDiv = Renderer::getDom()->createElement("div");
        $boardManagerDiv->setAttribute("id", "boardManager");

        $boardRow1ManagerDiv = Renderer::getDom()->createElement("div");
        $boardRow1ManagerDiv->setAttribute("class", "boardRowManager");
        $boardRow1ManagerDiv->appendChild($this->renderLine(-1,true));


        $boardRow2ManagerDiv = Renderer::getDom()->createElement("div");
        $boardRow2ManagerDiv->setAttribute("class", "boardRowManager");
        $boardRow2ManagerDiv->appendChild($this->renderCol(true));

        $boardRow3ManagerDiv = Renderer::getDom()->createElement("div");
        $boardRow3ManagerDiv->setAttribute("class", "boardRowManager");
        $boardRow3ManagerDiv->appendChild($this->renderLine(-1,false));

        $boardManagerDiv->appendChild($boardRow1ManagerDiv);
        $boardManagerDiv->appendChild($boardRow2ManagerDiv);
        $boardManagerDiv->appendChild($boardRow3ManagerDiv);



        $boardDiv = Renderer::getDom()->createElement("div");
        $boardDiv->setAttribute("id", "board");

        for($y = 0; $y < self::SIZE; $y++){
            $boardDiv->appendChild($this->renderLine($y, $turn));
        }
        $boardRow2ManagerDiv->appendChild($boardDiv);

        $boardRow2ManagerDiv->appendChild($this->renderCol(false));

        return $boardManagerDiv;
    }
}