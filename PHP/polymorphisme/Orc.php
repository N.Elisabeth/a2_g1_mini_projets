<?php

require_once("./Character.php");

class Orc extends Character{

    public $nomDuMarteau;

    public function __construct($nom, $nomDuMarteau)
    {
        parent::__construct($nom);
        $this->nomDuMarteau = $nomDuMarteau;

    }


    public function attaquer(){
        echo "Je casse la tête de l'elf avec ".$this->nomDuMarteau."<br/>";
    }
}