<?php


abstract class Character {

    private $nom;

    public function __construct($nom)
    {
        $this->nom = $nom;
    }

    public function direSonNom(){
        echo "Je me nomme ".$this->nom."<br/>";
    }

    public abstract function attaquer();
}