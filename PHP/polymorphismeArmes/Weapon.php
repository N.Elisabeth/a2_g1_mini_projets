<?php

abstract class Weapon {

    public abstract function use();
}