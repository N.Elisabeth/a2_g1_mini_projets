<?php


class PDOManager
{

    const DRIVER = "mysql";
    const HOST = "host=127.0.0.1:8889";
    const USERNAME = "root";
    const PASSWORD = "ROOT";
    const DBNAME = "rpg";

    /*
     * @var PDOManager
     */
    static $_instance;

    /**
     * @var PDO
     */
    public $pdo;

    public function __construct()
    {
        $this->pdo = new PDO(self::DRIVER.":".self::HOST.";dbname=".self::DBNAME,self::USERNAME, self::PASSWORD);
    }

    public static function getInstance(): PDOManager
    {
        if(self::$_instance == null){
            self::$_instance = new PDOManager();
        }
        return self::$_instance;
    }

    public static function fetchAll($table, $params = null, $where = null): array
    {
        $query = "SELECT * FROM ".$table;
        $request = self::getInstance()->pdo->prepare($query);
        $request->execute();
        return $request->fetchAll();
    }

    public static function insert($table, $paramsNames, $values){
        $query = "INSERT INTO ".$table." (";
        for ( $i = 0; $i < count($paramsNames); $i ++){
            $query .=  $paramsNames[$i].($i == count($paramsNames) - 1 ? "" : ",");
        }
        $query .= ") VALUES (";

        for ( $i = 0; $i < count($paramsNames); $i ++){
            $query .=  "?".($i == count($paramsNames) - 1 ? "" : ",");
        }
        $query .= ");";

        $request = self::getInstance()->pdo->prepare($query);
        self::getInstance()->pdo->beginTransaction();
        $request->execute($values);
        self::getInstance()->pdo->commit();
    }
     public static function update($sqlRequest, $param){
        $request = self::getInstance()->pdo->prepare($sqlRequest);
        self::getInstance()->pdo->beginTransaction();
        $request->execute($param);
        self::getInstance()->pdo->commit();
    }

    public static function delete($table, $id){
        $request = self::getInstance()->pdo->prepare("DELETE FROM ? WHERE `id`= ?");
        echo $table."\n";
        $request->execute([
            $table,
            $id
        ]);
        return $request->fetch();
    }

}