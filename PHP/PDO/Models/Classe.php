<?php


class Classe
{

    /**
     * @var mixed
     */
    private $id;
    /**
     * @var mixed
     */
    private $nom;

    public function __construct($data)
    {
        $this->id = $data["idClasse"];
        $this->nom = $data["nom"];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }
    public function ToJson(){
        return [
            "id" => $this->id,
            "nom" => $this->nom
        ];
    }

}