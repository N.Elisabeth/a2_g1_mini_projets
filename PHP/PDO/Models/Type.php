<?php


class Type
{


    /**
     * @var mixed
     */
    private $id;
    /**
     * @var mixed
     */
    private $nom;
    /**
     * @var mixed
     */
    private $description;

    public function __construct($data)
    {
        $this->id = $data["id"];
        $this->nom = $data["nom"];
        $this->description = $data["description"];
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }
}