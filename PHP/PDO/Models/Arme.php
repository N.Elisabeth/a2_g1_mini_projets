<?php


class Arme
{
    /**
     * @var mixed
     */
    private $nom;
    /**
     * @var mixed
     */
    private $description;
    /**
     * @var mixed
     */
    private $puissance;
    /**
     * @var mixed
     */
    private $taux_de_penetration;
    /**
     * @var mixed
     */
    private $rareter;
    /**
     * @var mixed
     */
    private $niveau_pour_equiper;
    /**
     * @var mixed
     */
    private $id;

    public function __construct($data)
    {
        $this->id = $data["idArme"];
        $this->nom = $data["nom"];
        $this->description = $data["description"];
        $this->puissance = $data["puissance"];
        $this->taux_de_penetration = $data["taux_de_penetration"];
        $this->rareter = $data["rareter"];
        $this->niveau_pour_equiper = $data["niveau_pour_equiper"];
    }

    public function ToJson(){
        return [
            "id" => $this->id,
            "nom" => $this->nom,
            "description" => $this->description,
            "puissance" => $this->puissance,
            "taux_de_penetration" => $this->taux_de_penetration,
            "rareter" => $this->rareter,
            "niveau_pour_equiper" => $this->niveau_pour_equiper,
        ];
    }

}