<?php
header('Content-type: application/json');

require_once ("PDOManager.php");
require_once ("Models/Type.php");
require_once ("Models/Classe.php");
require_once ("Models/Arme.php");
/*
$classes = [
    "Archer",
    "Mage",
    "MageBlanc",
    "MageNoir",
    "Assasin",
    "Guerrier"
];

foreach ($classes as $class) {
    PDOManager::insert("classe", ["nom"], [$class]);
}
*/
/*
$datas = [
    [5, 1],
    [6, 2]
];
$params = ["classe","arme"];
foreach ($datas as $data) {
    PDOManager::insert("class_arme", $params, $data);
}
*/



$results = PDOManager::fetchAll("class");
$classes = [];

foreach ($results as $result){
    array_push($classes, (new Arme($result))->ToJson());
}
echo json_encode($classes, true);
?>