<?php


class Animale {
    protected $nom;
    private $age;
    private $race;

    public function __construct($nom, $age, $race)
    {
        $this->nom = $nom;
        $this->age = $age;
        $this->race = $race;

    }

    public function marcher(){
        echo $this->nom." est en train de marcher <br/>";
    }
}