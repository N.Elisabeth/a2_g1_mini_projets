<?php
include "heritage/Animale.php";
include "heritage/Felin.php";
include "heritage/Panthere.php";
include "heritage/Chat.php";
include "heritage/Chien.php";


$animale = new Animale("grizouille", 7, "gris");
var_dump($animale);
echo "<br/>";
$chat = new Chat("grizouille", 7, "gris");
$chat->marcher();
$chat->miauler();
var_dump($chat);


echo "<br/>";
$chien = new Chien("Miranda", 5, "berger allemand");
$chien->marcher();
$chien->aboyer();
var_dump($chien);


echo "<br/>";
$panthere = new Panthere("Bagherra", 40, "noir");
$panthere->marcher();
$panthere->miauler();
var_dump($panthere);