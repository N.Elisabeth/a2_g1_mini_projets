<?php


abstract class Character implements Movable {

    private $nom;

    public function __construct($nom)
    {
        $this->nom = $nom;
    }

    public function direSonNom(){
        echo "Je me nomme ".$this->nom."<br/>";
    }

    public abstract function attaquer();

    public function move($x, $y)
    {
        echo "Move to".$x." : ".$y;
    }
}