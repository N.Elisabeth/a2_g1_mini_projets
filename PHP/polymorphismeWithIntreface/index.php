<?php
include ("./Orc.php");
include("./ElfNoir.php");
include("./ElfBlanc.php");


$orc = new Orc("zorc", "Marteau en bronze");
$orc->direSonNom();
$orc->attaquer();

/*
$elf = new Elf("Ellie");
$elf->direSonNom();
$elf->attaquer();
*/

$elf = new ElfNoir("Ellie");
$elf->direSonNom();
$elf->attaquer();
$elf->tirerArc("noir");
$elf->faireDeLaMagie();

$elf->move(10, 10);

$elfblanc = new ElfBlanc("Mickey");
$elfblanc->direSonNom();
$elfblanc->attaquer();
$elfblanc->tirerArc("rose");
$elfblanc->faireDeLaMagie();
