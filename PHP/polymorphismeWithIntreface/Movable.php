<?php


interface Movable
{
    function move($x, $y);
}