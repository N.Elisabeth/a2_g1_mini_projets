<?php

class ColorList{
    const RED = "#FF0000";
    const GREEN = "#00FF00";
    const BLUE = "#0000FF";
}


class Voiture {

    private $immatriculation;

    private $couleur;

    private $nombreDeRoue = 4;

    private $vitesseMax;

    private $vitessCourante = 0;

    private $state = "off";

    public function __construct($immatriculation, $couleur, $nombreDeRoue, $vitesseMax)
    {
        $this->immatriculation = $immatriculation;
        $this->couleur = $couleur;
        $this->nombreDeRoue = $nombreDeRoue;
        $this->vitesseMax = $vitesseMax;
    }

    public function getVitesseCourante(){
        return $this->vitessCourante;
    }

    public function demarrer(){
        $this->state = "on";
    }

    public function accelerer(){
        $this->vitessCourante += 10;
        return $this->vitessCourante;
    }
}
