<?php
$div = DOMAccess::getDom()->createElement('div');
$div->setAttribute('style', "background-color:salmon; width:75px; height:75px");

echo DOMAccess::getDom()->saveHTML($div);


class DOMAccess{
    /**
     * @var DOMAccess
     */
    private static $_instance;

    /**
     * @var DOMDocument
     */
    private $dom;

    public function __construct()
    {
        $this->dom = new DOMDocument();
    }

    /**
     * @return DOMDocument
     */
    public static function getDom(): DOMDocument
    {
        if(self::$_instance == null){
            self::$_instance = new DOMAccess();
        }

        return self::$_instance->dom;
    }
}