// STATEMENTS 
/*
if(){

}else if{

}else{

}
*/

/*
while(){

}
for(){

}
do{

}()
*/

/*
switch(){
    case:
        break
    default:
        break
}
*/

function nomDeLaFunction() {
    console.log("JE SUIS UNE FONCTION")
}
nomDeLaFunction = function(){

}

functionFlecher = () => {
    console.log("JE SUIS UNE FLECHER")
}

var variableVarModifiable = 10
let variableModifiable = 10
const variableConst = 50

const monBeauTableau = []
// console.log(monBeauTableau)
monBeauTableau.push("Rio")
// console.log(monBeauTableau)

let monBeauTableauB = [...monBeauTableau]
monBeauTableauB.push("Pringles")
// console.log("monBeauTableau: ", monBeauTableau)
// console.log("monBeauTableauB :", monBeauTableauB)





monBeauTableauB.forEach(function (item, index) {
    // console.log(index + " : " + item)
})
monBeauTableauB = monBeauTableauB.map(function (item, index) {
    return (index + " : " + item)
})
// console.log(monBeauTableauB)



const monBelObjet = {}
// console.log(monBelObjet)
monBelObjet["nomDeLaClef"] = "ViewSonic"
// console.log(monBelObjet)
const monBelObjetB = { ...monBelObjet }
monBelObjetB["nomDeLaClef2"] = "OnePlus"
// console.log("monBelObjet :", monBelObjet)
// console.log("monBelObjetB :", monBelObjetB)


let keys = Object.keys(monBelObjetB)
// console.log(keys)
keys.forEach(function (key, index) {
    // console.log(index + " : " + key + " : " + monBelObjetB[key] )
})

let values = Object.values(monBelObjetB)
// console.log(values)



function carrer(obj) {
    const carrer = {
        positionX: obj.hasOwnProperty("positionX") ? obj.positionX : 0,
        positionY: obj.hasOwnProperty("positionY") ? obj.positionY : 0,
        longueur: obj.hasOwnProperty("longueur") ? obj.longueur : 50,
        hauteur: obj.hasOwnProperty("hauteur") ? obj.hauteur : 50,
        air: function () {
            return carrer.longueur * carrer.hauteur
        },
        perimetre: function () {
            return carrer.longueur * 2 + carrer.hauteur * 2
        },
        moveTo: function (positionX, positionY) {
            carrer.positionX = positionX
            carrer.positionY = positionY
        },
        toJson: function(){
            return {
                positionX: carrer.positionX,
                positionY: carrer.positionY,
                longueur: carrer.longueur,
                hauteur: carrer.hauteur,
            }
        }
    }
    return carrer
}

const carrer1Object = carrer({})
// console.log("carrer1Object :", carrer1Object)
const carrer2 = carrer(carrer1.toJson())

/*carrer2.moveTo(10, 20)
console.log("carrer1 :", carrer1)
console.log("carrer2 :", carrer2)
*/


class Carrer{
    constructor(obj) {
        this.__positionX = obj.hasOwnProperty("positionX") ? obj.positionX : 0;
        this.__positionY = obj.hasOwnProperty("positionY") ? obj.positionY : 0;
        this.__longueur = obj.hasOwnProperty("longueur") ? obj.longueur : 50;
        this.__hauteur = obj.hasOwnProperty("hauteur") ? obj.hauteur : 50;
    }
    air(){
        return this.__longueur * this.__hauteur
    }
    perimetre () {
        return this.__longueur * 2 + this.__hauteur * 2
    }
    moveTo (positionX, positionY) {
        this.__positionX = positionX
        this.__positionY = positionY
    }
    
    toJson(){
        return {
            positionX: this.__positionX,
            positionY: this.__positionY,
            longueur: this.__longueur,
            hauteur: this.__hauteur,
        }
    }
}

/*

const carrer1Class = new Carrer({})
console.log("carrer1Class :", carrer1Class)
console.log(carrer1Class.perimetre())
const carrer2 = new Carrer(carrer1Class.toJson())
console.log("carrer2 :", carrer2)
carrer2.moveTo(10, 20)
console.log("carrer1 :", carrer1Class)
console.log("carrer2 :", carrer2)

*/

/*
* Objet cercle
* position X et position Y => Centre du cercle
* R => rayon du cercle
* */

function vector2(data){
    const vector = {
        x: data.x,
        y: data.y
    }
    return vector
}

function circle(data) {
    const circle = {
        center: data.position,
        rayon: data.rayon,
        diametre: function () { return circle.rayon * 2 },
        perimetre: function () { return 2 * Math.PI * circle.rayon },
        air: function () { return Math.PI * Math.pow(circle.rayon, 2) },
        moveTo: function(vector){
            circle.center = vector
        },
        toJson: function () {
            return {
                position: circle.center,
                rayon: circle.rayon,
            }
        }
    }
    return circle
}
const circle1 = circle({position: vector2({ x:10, y:10}), rayon:  50})
console.log(circle1.toJson())
const circle2 = circle(circle1.toJson())
circle2.moveTo(vector2({ x:40, y:20}))
console.log("circle1 :", circle1.air())
console.log("circle1 :", circle1)
console.log("circle2 :", circle2.air())


