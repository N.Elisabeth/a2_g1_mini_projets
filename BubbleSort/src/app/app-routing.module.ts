import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BubbleSortComponent } from "./Pages/bubble-sort/bubble-sort.component";

const routes: Routes = [
  { path:"bubble-sort", component: BubbleSortComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
