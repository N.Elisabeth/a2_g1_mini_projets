import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bubble-sort',
  templateUrl: './bubble-sort.component.html',
  styleUrls: ['./bubble-sort.component.scss']
})
export class BubbleSortComponent implements OnInit {

  title = "Je suis un titre"

  colorTab: number[] = []
  sortingInterval: any = null
  currentRound: number = 0
  currentIndex: number = 0
  counter: number = 360

  /**
   * un tableau =>
   * [ 8, 6, 5, 4, 9, 1 ]
   * [ 6, 8, 5, 4, 9, 1 ]
   * [ 6, 5, 8, 4, 9, 1 ]
   * [ 6, 5, 4, 8, 9, 1 ]
   * [ 6, 5, 4, 8, 1, 9 ]
   *  => Seconde itération
   *
   *  RGB => Red Green Blue => 255 * 255 * 255 => 3 * 8 bit
   *  HSL => Hue Saturation Light
   *  Hue => 0 - 360 deg
   *  Sat => 0 - 100 %
   *  Lig => 0 - 100%
   */



  constructor() {
    this.colorTab = []
    while(this.colorTab.length < this.counter){
      let col = Math.floor(Math.random() * 360)
      if(!this.colorTab.includes(col))
          this.colorTab.push(col)
    }
  }

  ngOnInit(): void {

    this.sortingInterval = setInterval(() => {
      this.sortingManager()
    }, 1)
  }

  sortingManager(){

    if(this.currentIndex + 1 < this.counter - this.currentRound && this.colorTab[this.currentIndex] > this.colorTab[this.currentIndex + 1]){
      let temp = this.colorTab[this.currentIndex]
      this.colorTab[this.currentIndex] = this.colorTab[this.currentIndex + 1]
      this.colorTab[this.currentIndex + 1] = temp
    }

    this.currentIndex ++
    if(this.currentIndex >= this.counter - 1 - this.currentRound){
      this.currentRound ++
      this.currentIndex = 0
    }

    if(this.currentRound >= this.counter - 1){
      clearInterval(this.sortingInterval)
    }

  }

}
