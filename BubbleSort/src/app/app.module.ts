import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BubbleSortComponent } from './Pages/bubble-sort/bubble-sort.component';
import { BubbleComponent } from './Components/bubble/bubble.component';

@NgModule({
  declarations: [
    AppComponent,
    BubbleSortComponent,
    BubbleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
