class CardModel {
    
    /**
     * @param data { object }
     */
    constructor(data) {
        
        this.__sujet = new Sujet("[CardModel] : [ " +data["lastName"] +" ]")
        
        this.__id = data["id"]
        this.__firstName = data["firstName"]
        this.__lastName = data["lastName"]
        this.__age = data["age"]
        this.__phone = data["phone"]
        this.__description = data["description"]
        
        this.__open = false
    }
    
    /**
     * @param attributeName { string }
     * @return { string }
     */
    getAttribute(attributeName){
        return this["__"+attributeName]
    }
    
    /**
     * @return {Sujet}
     */
    getSujet(){
        return this.__sujet
    }
    
    /**
     * @return {boolean}
     */
    isOpen(){ return this.__open }
    
    switchOpenState(){
        this.__open = ! this.__open
        this.__sujet.notify("switch")
    }
}
