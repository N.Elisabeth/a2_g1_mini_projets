class CardController {
    
    /**
     * @param cardModel { CardModel }
     * @param cardView { CardView}
     */
    constructor(cardModel, cardView) {
        this.__model = cardModel
        
        const observerName = "[CardController] : [Observer] : ["+ cardModel.getAttribute("lastName") + "]"
        const observerCallback = (data) => { this.update(data)}
        this.__observer = new Observer( observerName, observerCallback)
        
        cardView.getSujet().attach(this.__observer)
    }
    
    update(data){
        this.__model.switchOpenState()
        console.log(data)
    }
}
