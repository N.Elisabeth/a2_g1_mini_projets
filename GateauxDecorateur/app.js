let mainContainer = document.getElementById("mainContainer")

let shapes = []
shapes.push(
   new BorderManager(new Rectangle(mainContainer), "10px", "solid", "#555")
)

shapes.push(
    new BorderManager(
        new BoxShadowManager(new Cercle(mainContainer), "-5px", "-5px", "10px", "10px", "#345"),
        "10px", "solid", "#789")
)

shapes.forEach((shape) => {
    shape.draw()
})
