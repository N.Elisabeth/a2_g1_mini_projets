class Rectangle extends Shape {
    
    
    /**
     * @param parent { HTMLElement }
     */
    constructor(parent) {
        console.warn("[RECTANGLE CONSTRUCTOR]")
        super(parent);
    }

    draw() {
        this.__container = document.createElement("div")
        this.__container.classList.add('rectangle')
        
        this.__parent.appendChild(this.__container)
    }
}
