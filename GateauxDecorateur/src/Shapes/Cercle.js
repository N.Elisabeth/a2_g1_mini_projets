class Cercle extends Shape{
    
    
    /**
     * @param parent { HTMLElement }
     */
    constructor(parent) {
        super(parent);
    }
    
    
    draw() {
        this.__container = document.createElement("div")
        this.__container.classList.add('cercle')
    
        this.__parent.appendChild(this.__container)
    }
}
