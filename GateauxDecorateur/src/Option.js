class Option extends Shape{
    /**
     * @param shape { Shape }
     */
    constructor(shape) {
        // Constructor de Shape
        super(null);
        
        this.__shape = shape
    }
    
    getContainer() {
        return this.__shape.getContainer();
    }
}
