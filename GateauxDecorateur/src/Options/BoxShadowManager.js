class BoxShadowManager extends Option{
    
    /**
     * @param shape { Shape }
     * @param shiftRight { string }
     * @param shiftDown { string }
     * @param blur { string }
     * @param spread { string }
     * @param color { string }
     */
    constructor(shape, shiftRight, shiftDown, blur,spread , color) {
        console.warn("[BoxShadowManager CONSTRUCTOR]")
        super(shape);
        this.__shiftRight = shiftRight
        this.__shiftDown = shiftDown
        this.__spread = spread
        this.__blur = blur
        this.__color = color
    }
    
    
    draw() {
        this.__shape.draw()
        this.__shape.getContainer().style.boxShadow = `${this.__shiftRight} ${this.__shiftDown} ${this.__blur} ${this.__spread} ${this.__color}`
    }
}
