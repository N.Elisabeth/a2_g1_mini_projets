class BorderManager extends Option{
    
    /**
     * @param shape { Shape }
     * @param borderWidth { string }
     * @param borderStyle { string }
     * @param borderColor { string }
     */
    constructor(shape, borderWidth, borderStyle, borderColor) {
        console.warn("[BorderManager CONSTRUCTOR]")
        super(shape);
        
        this.__borderWidth = borderWidth
        this.__borderStyle = borderStyle
        this.__borderColor = borderColor
    }
    
    draw() {
        this.__shape.draw()
        this.__shape.getContainer().style.border = this.__borderWidth + " " + this.__borderStyle + " " + this.__borderColor
    }
}
