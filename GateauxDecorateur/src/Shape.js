class Shape{
    
    /**
     * @param parent { HTMLElement }
     */
    constructor(parent) {
        this.__parent = parent
        this.__container = null
        // console.log(this.__parent)
    }
    
    /**
     * @return { HTMLElement | null}
     */
    getContainer(){
        return this.__container
    }
    
    draw(){}

}
