
function RoundedSquare(data) {
    data = data === undefined ? {} : data
    
    const element = document.createElement("div")
    element.classList.add("roundedSquare")
    
    if(data.hasOwnProperty("size")){
        element.style.width = data.size
        element.style.height = data.size
    }
    
    if(data.hasOwnProperty("borderRadius")){
        element.style.borderRadius = data.borderRadius
    }
    
    if(data.hasOwnProperty("parent"))
        data.parent.appendChild(element)
    
    const roundedSquare = {
        element: element,
        /**
         * @param parent { HTMLElement }
         */
        setParent: function(parent){
            parent.appendChild(roundedSquare.element)
        }
    }
    return roundedSquare
}

function Circle(data) {
    data = data === undefined ? {} : data
    
    const element = document.createElement("div")
    element.classList.add("roundedSquare")
    
    if(data.hasOwnProperty("size")){
        element.style.width = data.size
        element.style.height = data.size
        element.style.borderRadius = data.size
    }
    
    if(data.hasOwnProperty("parent"))
        data.parent.appendChild(element)
    
    const circle = {
        element: element,
        /**
         * @param parent { HTMLElement }
         */
        setParent: function(parent){
            parent.appendChild(circle.element)
        }
    }
    return circle
}

function Diamond(data) {
    data = data === undefined ? {} : data
    
    const element = document.createElement("div")
    element.classList.add("roundedSquare")
    
    if(data.hasOwnProperty("size")){
        element.style.width = data.size
        element.style.height = data.size
    }
    
    if(data.hasOwnProperty("parent"))
        data.parent.appendChild(element)
    
    
    if(data.hasOwnProperty("borderRadius")){
        element.style.borderRadius = data.borderRadius
    }
    
    element.style.transform = "rotate(45deg)"
    
    const diamond = {
        element: element,
        /**
         * @param parent { HTMLElement }
         */
        setParent: function(parent){
            parent.appendChild(circle.element)
        }
    }
    return diamond
}


class Hexagone{
    constructor(data) {
        this.__hexagoneContainer = document.createElement("div")
        this.__hexagoneContainer.style.display = "flex"
        
        this.__parts = {
            left: document.createElement("div"),
            middle: document.createElement("div"),
            right: document.createElement("div"),
        }
        
        this.__parts.left.style.borderBottom = data.height / 2 + "px solid transparent"
        this.__parts.left.style.borderTop = data.height / 2 + "px solid transparent"
        this.__parts.left.style.borderRight =  (data.height*0.3) + "px solid " + data.backColor
    
        this.__parts.middle.style.height = data.height + "px"
        this.__parts.middle.style.width = (data.height*0.6) + "px"
        this.__parts.middle.style.backgroundColor =  data.backColor
        this.__parts.middle.style.display = "flex"
        this.__parts.middle.style.alignItems = "center"
        
        this.__title = document.createElement("p")
        this.__title.innerText = data.title
        this.__title.style.textAlign = "center"
        this.__title.style.margin = "0px"
        this.__parts.middle.appendChild(this.__title)
        
    
        this.__parts.right.style.borderBottom = data.height / 2 + "px solid transparent"
        this.__parts.right.style.borderTop = data.height / 2 + "px solid transparent"
        this.__parts.right.style.borderLeft =  (data.height*0.3) + "px solid " + data.backColor
        
        Object.keys(this.__parts).forEach(key => {
            this.__hexagoneContainer.appendChild(this.__parts[key])
        })
        
        data.parent.appendChild(this.__hexagoneContainer)
    }
}

const container = document.getElementById("lineContainer")
// 0 1 2 3 4

// (Math.floor(Math.random() * 10) + 1) * 20

for(let i = 0; i < 5; i ++){
    //new Hexagone({parent: container, height: 200, backColor: "sandybrown", title: "Je suis un hexagone"})
}



const shapes = [
    Circle({size: "200px", parent: container }),
    RoundedSquare({size: "200px", parent: container, borderRadius: "5px" }),
    Diamond({size: "150px", parent: container, borderRadius: "5px" }),
    RoundedSquare({size: "200px", parent: container, borderRadius: "5px" }),
    Circle({size: "200px", parent: container })
]



