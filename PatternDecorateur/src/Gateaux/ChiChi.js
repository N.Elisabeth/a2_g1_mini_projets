class ChiChi extends Gateau{
    
    /**
     * @param prix { number }
     */
    constructor(prix) {
        super(prix, "ChiChi");
    }
    
    getPrix() {
        return super.getPrix();
    }
    
}
