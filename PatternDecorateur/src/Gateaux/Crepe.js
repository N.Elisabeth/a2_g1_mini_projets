class Crepe extends Gateau {
    
    
    /**
     * @param prix { number }
     */
    constructor(prix) {
        super(prix, "Crepe");
    }
    
    getPrix() {
        return super.getPrix();
    }
}
