class Option extends Gateau{
    /**
     * @param gateau { Gateau }
     * @param prix { number }
     */
    constructor(gateau, prix) {
        // Constructor de Gateau
        super(prix);
        this.__gateau = gateau
    }
    
    getPrix() {
        return this.__gateau.getPrix() + this.__prix
    }
}
