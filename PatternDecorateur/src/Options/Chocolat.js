class Chocolat extends Option{
    
    /**
     * @param gateau { Gateau }
     * @param prix { number }
     */
    constructor(gateau, prix) {
        super(gateau, prix);
    }
    
    getPrix() {
        let prix = super.getPrix()
        console.log("+ Chocolat : " + this.__prix + " Euros")
        return prix
    }
}
