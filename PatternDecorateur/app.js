let mainContainer = document.getElementById("mainContainer")

/**
 * @type {Gateau[]}
 */
let gateaux = []
gateaux.push(
    new Chocolat(new ChiChi(5), 5)
)

gateaux.forEach((gateau) => {
    console.log(gateau.getPrix() + " Euros")
})
