
function stateMachine(statesDefinition){
    if(statesDefinition.hasOwnProperty("initialState") === false)
        return null
    const machine = {
        currentState: statesDefinition.initialState,
        transition: function(event){
            
            const currentStateDefinition =  statesDefinition[machine.currentState]
            const currentStateTransition =  currentStateDefinition.transitions[event]
            
            if(!currentStateTransition){
                return
            }
            
            const destinationDefinition = statesDefinition[currentStateTransition.target]
            
            currentStateTransition.action()
            
            if(currentStateDefinition.hasOwnProperty("actions") && currentStateDefinition.actions.hasOwnProperty("onExit"))
                currentStateDefinition.actions.onExit()
            
            if(destinationDefinition.hasOwnProperty("actions") && destinationDefinition.actions.hasOwnProperty("onEnter"))
                destinationDefinition.actions.onEnter()
            
            
            machine.currentState = currentStateTransition.target
            return machine.currentState
        }
    }
    return machine;
}



/*
states = {
    initialState: 'on',
    on:{
        actions: {
            onEnter: function () {
                console.log("on: onEnter")
            },
            onExit: function () {
                console.log("on: onExit")
            }
        },
        transitions: {
            "switch": {
                target: "off",
                action: function () {
                    console.log("on passe à OFF")
                }
            }
        }
    },
    off:{
        actions: {
            onEnter: function () {
                console.log("off: onEnter")
            },
            onExit: function () {
                console.log("off: onExit")
            }
        },
        transitions: {
            "switch": {
                target: "on",
                action: function () {
                    console.log("on passe à ON")
                }
            }
        }
    }
}
*/
/*
statesFeu = {
    initialState: "vert",
    vert: {
        transitions: {
            'switch':{
                target:'orange',
                action: function () {
                    console.log("On passe au orange")
                }
            }
        }
    },
    orange:{
        transitions: {
            'switch':{
                target:'rouge',
                action: function () {
                    console.log("On passe au rouge")
                }
            }
        }
    },
    rouge:{
        transitions: {
            'switch':{
                target:'vert',
                action: function () {
                    console.log("On passe au vert")
                }
            }
        }
    }
}
*/
