class Observer {
    
    __name = ""
    __callback = () => { console.log("Je suis une belle callback") }
    
    /**
     * @param name { string }
     * @param callback { Function }
     */
    constructor(name, callback = () => { console.log("Je suis une belle callback") }) {
        this.__name = name
        this.__callback = callback
    }
    
    /**
     * @return {string}
     */
    getName(){
        return this.__name
    }
    
    /**
     * @param message { string }
     */
    onObserve(message){
        console.log(`[ ${this.__name} ] : OnObserve`)
        this.__callback(message)
    }
}
