class Sujet {

    __name = ""
    /**
     * @type { Observer[] }
     * @private
     */
    __observers = []
    
    /**
     * @param name { string }
     */
    constructor(name) {
        this.__name = name
    }
    
    
    /**
     * @param observer { Observer }
     */
    attach(observer){
        this.__observers.push(observer)
    }
    
    /**
     * @param observer { Observer }
     */
    detach(observer){
        const index = this.__observers.findIndex((currObserver) => {
            return observer.getName() === currObserver.getName()
        })
        if(index >= 0){
          this.__observers.splice(index, 1)
        }
    }
    
    /**
     * @param message { string }
     */
    notify(message){
        console.log(`[ ${this.__name} ] : notify`)
        this.__observers.forEach((observer) => {
            observer.onObserve(message)
        })
    }
}
