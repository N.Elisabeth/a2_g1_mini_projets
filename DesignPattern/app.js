
/*
const sujet1 = new Sujet("Sujet1")

const observer = new Observer("Observer1", (data) => { console.log(data)})
const observer2 = new Observer("Observer2", (data) => { console.log(data)})

sujet1.attach(observer)
sujet1.attach(observer2)
sujet1.notify("Bonjour")

sujet1.detach(observer)
sujet1.notify("Hello")
*/



class Block extends Sujet{
    constructor() {
        super("Block")
        this.__color = "white"
    }
    
    /**
     * @return {string}
     */
    getColor(){
        return this.__color
    }
    
    /**
     * @param color {string}
     */
    setColor(color){
        this.__color = color
        this.notify("")
    }
}

class BlockRenderer extends Observer{

    constructor() {
        super("BlockRenderer",(data) => {this.onCallback()});
    }
    
    onCallback(){
        const element = document.getElementById("Render1")
        element.style.backgroundColor = "#" + block.getColor()
    }
}




const block = new Block()
const blockRenderer = new BlockRenderer()

block.attach(blockRenderer)


function onClick() {
    block.setColor(Math.floor(Math.random()*16777215).toString(16))
}
