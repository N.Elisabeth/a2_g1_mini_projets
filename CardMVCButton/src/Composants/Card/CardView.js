class CardView {
    /**
     * @param cardModel { CardModel }
     */
    constructor(cardModel) {
        
        this.__model = cardModel
        
        const observerName = "[CardView] : [Observer] : [" + cardModel.getAttribute("lastName") + "]"
        const observerCallback = (data) => {
            this.render(data)
        }
        this.__observer = new Observer(observerName, observerCallback)
        
        this.__model.getSujet().attach(this.__observer)
        
        
        const sujetName = "[CardView] : [Sujet] : [" + cardModel.getAttribute("lastName") + "]"
        this.__sujet = new Sujet(sujetName)
        
        this.__cardContainer = null
        
        this.__buttonView = new ButtonView(this.__model.getButton())
        
        this.createHtml()
    }
    
    /**
     * @return { ButtonView }
     */
    getButtonView() { return this.__buttonView }
    
    /**
     * @return {Observer}
     */
    getObserver() {
        return this.__observer
    }
    
    /**
     * @return {Sujet}
     */
    getSujet() {
        return this.__sujet
    }
    
    /**
     * @return { HTMLElement | null}
     */
    getContainer() {
        return this.__cardContainer
    }
    
    createHtml() {
        this.__cardContainer = document.createElement("div")
        this.__cardContainer.classList.add("cardContainer")
        
        this.generateHeader()
        this.generateBody()
        this.generateFooter()
    }
    
    generateHeader() {
        this.__headerContainer = document.createElement("div")
        this.__headerContainer.classList.add("headerContainer")
        
        this.__title = document.createElement("h1")
        this.__title.innerText = this.__model.getAttribute("firstName") + "-" + this.__model.getAttribute("lastName").toUpperCase()
        this.__title.classList.add("cardTitle");
        
        this.__headerContainer.appendChild(this.__title)
        
        this.__cardContainer.appendChild(this.__headerContainer)
        
    }
    
    generateBody() {
        this.__bodyContainer = document.createElement("div")
        this.__bodyContainer.classList.add("bodyContainer")
        
        this.__infosContainer = document.createElement("div")
        this.__infosContainer.classList.add("infosContainer")
        const infos = ["phone", "age"]
        
        infos.forEach((key) => {
            const infoContainer = document.createElement("p")
            infoContainer.innerText = key + " : " + this.__model.getAttribute(key)
            this.__infosContainer.appendChild(infoContainer)
        })
        
        this.__bodyContainer.appendChild(this.__infosContainer)
        this.__cardContainer.appendChild(this.__bodyContainer)
    }
    
    generateFooter() {
        this.__footerContainer = document.createElement("div")
        this.__footerContainer.classList.add("footerContainer")
        
        this.__footerContainer.appendChild(this.__buttonView.getContainer())
        this.__cardContainer.appendChild(this.__footerContainer)
    }
    
    hideDescription() {
        // Supprime le container de la description
        this.__bodyContainer.removeChild(this.__bodyContainer.lastChild)
        
        
        // change la state de l'affichage
        this.__displayDescription = false
        
        // Transition de retour de la div
        this.__cardContainer.classList.add('horizBack');
        this.__cardContainer.classList.remove('horizTranslate');
    }
    
    displayDescription() {
        // Affichage de la description
        this.__descriptionContainer = document.createElement("div")
        this.__descriptionContainer.classList.add("infosContainer")
        const infos = ["description"]
        
        infos.forEach((key) => {
            const infoContainer = document.createElement("p")
            infoContainer.innerText = key + " : " + this.__model.getAttribute(key)
            this.__descriptionContainer.appendChild(infoContainer)
        })
        
        this.__bodyContainer.appendChild(this.__descriptionContainer)
        
        // fait la transition d'ouverture
        if (this.__cardContainer.classList.contains('horizBack')) {
            // si déjà ouvert on supprimer la class de retour
            this.__cardContainer.classList.remove('horizBack');
        }
        this.__cardContainer.classList.add('horizTranslate');
        
    }
    
    /**
     * @param data { * }
     */
    render(data) {
        
        if (this.__model.isOpen() === true) {
            this.displayDescription()
        } else {
            this.hideDescription()
        }
        console.log(data)
    }
}
