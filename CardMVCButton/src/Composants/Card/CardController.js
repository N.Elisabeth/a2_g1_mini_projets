class CardController {
    
    /**
     * @param cardModel { CardModel }
     * @param cardView { CardView}
     */
    constructor(cardModel, cardView) {
        this.__model = cardModel
        
        const observerName = "[CardController] : [Observer] : ["+ cardModel.getAttribute("lastName") + "]"
        const observerCallback = (data) => { this.update(data)}
        this.__viewObserver = new Observer( observerName, observerCallback)
        
        cardView.getSujet().attach(this.__viewObserver)
        
        
        this.__buttonController = new ButtonController(this.__model.getButton(), cardView.getButtonView())
        const observerName2 = "[CardController] : [Observer] : [ButtonController]"
        const observerCallback2 = (data) => { this.update(data)}
        this.__buttonObserver = new Observer( observerName2, observerCallback2)
        this.__buttonController.getSujet().attach(this.__buttonObserver)
        
    }
    
    update(data){
        this.__model.switchOpenState()
        console.log(data)
    }
}
