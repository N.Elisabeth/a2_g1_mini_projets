class ButtonController {
    /**
     * @param buttonModel { ButtonModel }
     * @param buttonView { ButtonView }
     */
    constructor(buttonModel, buttonView) {
        
        this.__buttonModel = buttonModel
        
        this.__observer = new Observer("[ButtonController] : Observer", (data) => {this.update(data)})
        buttonView.getSujet().attach(this.__observer)
        
        this.__sujet = new Sujet("[ButtonController] : Sujet")
    }
    
    /**
     * @return {Sujet}
     */
    getSujet() { return this.__sujet }
    
    update(data){
        console.log(data)
        this.__buttonModel.setState(data)
        if(data === "click")
            this.__sujet.notify("")
    }
}
