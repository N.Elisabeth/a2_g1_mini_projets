class ButtonModel {
    
    /**
     * @param texts { Object }
     * @param colors { Object }
     */
    constructor(texts, colors) {
        this.__texts = texts
        this.__colors = colors
        
        this.__stateMachine = new StateMachine([
            new State("closeNormal", true)
                .addTransition( "mouseenter", new Transition("closeHover", () => {}))
                .addTransition( "click", new Transition("openNormal", () => {})),
            new State("closeHover", false)
                .addTransition( "mouseleave", new Transition("closeNormal", () => {}))
                .addTransition( "click", new Transition("openNormal", () => {})),
            new State("openNormal", false)
                .addTransition( "mouseenter", new Transition("openHover", () => {}))
                .addTransition( "click", new Transition("closeNormal", () => {})),
            new State("openHover", false)
                .addTransition( "mouseleave", new Transition("openNormal", () => {}))
                .addTransition( "click", new Transition("closeNormal", () => {})),
        ])
        
        this.__sujet = new Sujet(`[ButtonModel] : Sujet`)
    }
    
    /**
     * @return {Sujet}
     */
    getSujet(){ return this.__sujet }
    
    /**
     * @return {string}
     */
    getState() { return this.__stateMachine.getCurrentStateName() }
    
    /**
     * @param state {string}
     */
    setState(state) {
        this.__stateMachine.transition(state)
        this.__sujet.notify("")
    }
    
    /**
     * @param state { string }
     * @return {string}
     */
    getText(state){ return this.__texts[state] }
    
    /**
     * @param state {string}
     * @return {string}
     */
    getColor(state) { return this.__colors[state] }
    
    /**
     * @return {Object}
     */
    getColors() { return this.__colors }
    
}
