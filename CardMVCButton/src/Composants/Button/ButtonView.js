class ButtonView {
    
    /**
     * @param buttonModel { ButtonModel }
     */
    constructor(buttonModel) {
        this.__model = buttonModel
        this.__observer = new Observer("[ButtonView] : Observer", (data) => {this.render(data)})
        this.__model.getSujet().attach(this.__observer)
        
        this.__sujet = new Sujet("[ButtonView] : Sujet")
        
        this.__container = null
        
        this.createHtml()
    }
    
    /**
     * @return {Sujet}
     */
    getSujet(){ return this.__sujet }
    
    /**
     * @return {HTMLElement | null}
     */
    getContainer(){ return this.__container}
    
    createHtml(){
        this.__container = document.createElement("div")
        this.__container.setAttribute("id", "ButtonComposant")
        
        this.__buttonElement = document.createElement("button")
        this.__buttonElement.classList.add("buttonCard")
        this.__buttonElement.innerText = this.__model.getText(this.__model.getState())
        this.__buttonElement.style.backgroundColor = this.__model.getColor(this.__model.getState())
        
        this.__buttonElement.addEventListener('click',() =>{
            this.__sujet.notify('click')
        })
        
        this.__buttonElement.addEventListener('mouseenter',() =>{
            this.__sujet.notify('mouseenter')
        })
        
        this.__buttonElement.addEventListener('mouseleave',() =>{
            this.__sujet.notify('mouseleave')
        })
        
        this.__container.appendChild(this.__buttonElement)
    }
    
    render(data){
        console.log(data)
        this.__buttonElement.innerText = this.__model.getText(this.__model.getState())
        this.__buttonElement.style.backgroundColor = this.__model.getColor(this.__model.getState())
    }
}
