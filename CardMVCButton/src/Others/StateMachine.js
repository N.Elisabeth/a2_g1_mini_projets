class Transition{
    /**
     * @param targetName { string }
     * @param callback { Function }
     */
    constructor(targetName, callback) {
        this.__targetName = targetName
        this.__callback = callback
    }
    
    /**
     * @return {string}
     */
    getTargetName(){
        return this.__targetName
    }
    
    /**
     * @return {Function}
     */
    getCallback(){
        return this.__callback
    }
}
class State{
    /**
     * @param stateName { string }
     * @param initialState { boolean }
     */
    constructor(stateName, initialState = false) {
        this.__transitions = {}
        this.__hooks = {}
        this.__stateName = stateName
        this.__initialState = initialState
    }
    
    /**
     * @param hookName { string }
     * @param callBack { Function }
     * @return { State }
     */
    addHook(hookName, callBack){
        this.__hooks[hookName] = callBack
        
        return this
    }
    
    /**
     *
     * @param hookName
     * @return { Function | null}
     */
    getHook(hookName){
        if(this.__hooks.hasOwnProperty(hookName))
            return this.__hooks[hookName]
        return null
    }
    
    /**
     * @return {string}
     */
    getStateName(){
        return this.__stateName
    }
    
    /**
     * @return { boolean }
     */
    getIsInitialState(){
        return this.__initialState
    }
    
    /**
     * @param transitionName { string }
     * @param transition { Transition }
     * @return { State }
     */
    addTransition(transitionName, transition){
        this.__transitions[transitionName] = transition
        return this
    }
    
    /**
     * @param transitionName { string}
     * @return { Transition | null }
     */
    getTransition(transitionName){
        if(this.__transitions.hasOwnProperty(transitionName))
            return this.__transitions[transitionName]
        return null
    }
}
class StateMachine{
    /**
     * @param states { State[] }
     */
    constructor(states) {
        this.__currentStateName = states.find(function (state) {
            return state.getIsInitialState()
        }).getStateName()
    
        this.__states = {}
        states.forEach((state) => {
            this.__states[state.getStateName()] = state
        })
    }
    
    transition(transitionName){
        const currentStateTransition =  this.__states[this.__currentStateName].getTransition(transitionName)
        
        if(!currentStateTransition){
            console.warn("la transition ["+transitionName+"] n'existe pas !")
            return
        }
        const destinationState = this.__states[currentStateTransition.getTargetName()]
        currentStateTransition.getCallback()()
    
        if(this.__states[this.__currentStateName].getHook("onExit") != null)
            this.__states[this.__currentStateName].getHook("onExit")()
        
        if(destinationState.getHook("onEnter") != null)
            destinationState.getHook("onEnter")()
        
    
        this.__currentStateName = currentStateTransition.getTargetName()
        return this.__currentStateName
    }
    
    /**
     * @return { string }
     */
    getCurrentStateName(){
        return this.__currentStateName
    }
}


const states = [
    new State("on", true)
        .addTransition("switchOff", new Transition("off", function () { console.log("on passe à OFF") }))
        .addHook("onEnter", function(){ console.log("Entrer dans ON")})
        .addHook("onExit", function(){ console.log("Sortir de ON")}),
    new State("off").addTransition("switchOn", new Transition("on", function () { console.log("on passe à ON") }))
        .addHook("onEnter", function(){ console.log("Entrer dans OFF")})
        .addHook("onExit", function(){ console.log("Sortir de OFF")})
]

const StatesMachines = new StateMachine(states)
console.log("StatesMachines.currentState :", StatesMachines.getCurrentStateName())
state = StatesMachines.getCurrentStateName()
state = StatesMachines.transition('switchOff')
console.log("StatesMachines.currentState :", StatesMachines.getCurrentStateName())
state = StatesMachines.transition('switchOn')
console.log("StatesMachines.currentState :", StatesMachines.getCurrentStateName())
