import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import User from "../../Models/User";

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {

  @Input('user') user: User|undefined
  @Output('sayHello') sayHello = new EventEmitter()


  constructor() {
    console.log(this.user)
  }

  ngOnInit(): void {
  }

  say(){
    this.sayHello.emit('toto')
  }

}
