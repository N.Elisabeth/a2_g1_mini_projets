import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../Services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent{

  title = "Connection"
  openInscription = false
  formulaire: FormGroup


  constructor(
    private userService: UserService,
    private router: Router
  ) {
    this.formulaire = new FormGroup({
      'email' : new FormControl('', [Validators.email, Validators.required]),
      'password' : new FormControl('', [Validators.required, Validators.minLength(8)]),
    })
  }

  get email() { return this.formulaire.get('email'); }
  get password() { return this.formulaire.get('password'); }

  onOpenInscription(){
    this.openInscription = !this.openInscription
  }

  valider(){
    if(this.formulaire.valid){
      let user = this.userService.findUser(this.formulaire.get('email')?.value)
      console.log(user)
      if(user){
        let passwordValid = user.password == this.formulaire.get('password')?.value
        if(passwordValid){
          this.userService.setCurrentUser(user)
          this.router.navigateByUrl("home")
        }
      }
    }
  }


}
