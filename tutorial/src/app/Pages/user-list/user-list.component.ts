import { Component, OnInit } from '@angular/core';
import {UserService} from "../../Services/user.service";
import User from "../../Models/User";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  userList: User[]

  constructor(
    private userService: UserService
  ) {
    this.userList = []
    this.userService.getUserList().then((userList) => {
      console.log(userList)
      this.userList = <User[]>userList
    })
  }

  sayHello(event: any){
    console.log(event)
  }

  ngOnInit(): void {
  }

}
