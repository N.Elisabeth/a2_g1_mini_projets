import { Injectable } from '@angular/core';
import User from "../Models/User";
import {Router} from "@angular/router";
import {ConnectionService} from "./connection.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userList: User[]
  currentUser: User

  constructor(
    private router: Router,
    private connectionService: ConnectionService
  ) {
    this.userList = []


    if(localStorage.getItem("currentUser")!= undefined)
      this.currentUser = new User(JSON.parse(<string>localStorage.getItem("currentUser")))
    else{
      this.currentUser = new User({})
      router.navigateByUrl('login')
    }
    this.getUserList()

  }

  async getUserList(){
    return new Promise((resolve, reject) => {
      if(this.userList.length == 0){
        this.connectionService.getRequest('data.json').then((data) => {
          let jsonData = []
          if (typeof data === "string") {
            jsonData = JSON.parse(data)
          }
          this.userList = jsonData.map((user: any) => { return new User(user)})
          console.log(this.userList)
          resolve(this.userList)
        })
      }else{
        resolve(this.userList)
      }
    })
  }

  findUser(email: string): User | undefined{
    console.log(this.userList)
    return this.userList.find((user) => {
      console.log(user.lastName + "." + user.firstName + "@gmail.com")
      return user.lastName + "." + user.firstName + "@gmail.com" == email
    })
  }

  setCurrentUser(user:User){
    this.currentUser = user
    localStorage.setItem("currentUser", JSON.stringify(this.currentUser.data))
  }

  getCurrentUser(){
    return this.currentUser
  }



}
