import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  constructor() { }

  async getRequest(fileName: string){
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest()
      xhr.open('GET', './assets/'+ fileName)
      xhr.send()
cd
      xhr.onload = function () {
        resolve(xhr.responseText)
      }
    })
  }
}
