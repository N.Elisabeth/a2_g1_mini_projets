
const screenDiv = document.getElementById("screen");
let currentOperation = ""
let firstTherme = 0;


function writeOnTheScreen(text) {
    if(screenDiv.innerText.length === 0 && text === '0'){
        return
    }
    screenDiv.innerText += text;
}

function eraseScreen() {
    screenDiv.innerText = ""
}

/**
 * @return {number}
 */
function getScreenValue() {
    return parseInt(screenDiv.innerText)
}

/**
 * @param operationName
 */
function setOperation(operationName) {
    currentOperation = operationName
    firstTherme = getScreenValue()
    eraseScreen()
}

function doCalcul() {
    let secondTherme = getScreenValue()
    eraseScreen()
    switch (currentOperation) {
        case "add":
            writeOnTheScreen((firstTherme + secondTherme).toString())
            break;
        case "remove":
            writeOnTheScreen((firstTherme - secondTherme).toString())
            break;
        case "multiply":
            writeOnTheScreen((firstTherme * secondTherme).toString())
            break;
        case "divid":
            writeOnTheScreen((firstTherme / secondTherme).toString())
            break;
    }
}
