const screen = document.getElementById("screen")
let currentOperation = "";
let termOne = "";

function handleButtonClick(event){
    const value = event.target.innerText
    
    if(screen.innerText.length === 0 && value === "0"){
        return
    }
    
    writeOnTheScreen(value)
}

function writeOnTheScreen(data){
    screen.innerText += data
}

function eraseScreen(){
    screen.innerText = ""
}

function saveScreen() {
    return screen.innerText
}

function setOperation(operationName) {
    currentOperation = operationName
    termOne = saveScreen()
    eraseScreen()
}

function doCalcul() {
    const termTwo = saveScreen()
    eraseScreen()
    switch (currentOperation) {
        case "multiply":
            writeOnTheScreen((termOne * termTwo).toString())
            break;
        case "add":
            writeOnTheScreen((termOne + termTwo).toString())
            break;
        case "divide":
            writeOnTheScreen((termOne / termTwo).toString())
            break;
        case "substract":
            writeOnTheScreen((termOne - termTwo).toString())
            break;
        case "binairy":
            writeOnTheScreen(binaryToNumber())
            break;
        case "descimal":
            writeOnTheScreen(numberToBinary())
            break;
    }
}

function binaryToNumber() {
    
    let number = 0
    let pow = 0;
    
    for(let i = termOne.length -1; 0 <= i ; i--){
        
        number += Math.pow(2, pow) * termOne[i]
        pow ++
    
    }
    
    return number
}

function numberToBinary() {
    
    let pow = 0
    while (termOne - Math.pow(2, pow) > 0){
        pow += 1
    }
    
    let string = "";
    for(let i = pow; i >= 0; i--){
        
        if(termOne - Math.pow(2, i) >= 0){
            string += "1"
            termOne = termOne - Math.pow(2, i)
        }else {
            string += "0"
        }
    }
    return string
}
